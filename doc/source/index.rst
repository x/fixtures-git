.. fixtures-git documentation master file, created by
   sphinx-quickstart on Fri Apr 13 12:20:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fixtures-git's documentation!
========================================

.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
