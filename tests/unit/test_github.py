# Copyright (c) 2018 Hewlett Packard Enterprise Development LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import mock
import testtools

from fixtures_git import github as gh_fixture


class TestGithubLogin(testtools.TestCase):

    @mock.patch('fixtures_git.github.github.enterprise_login',
                autospec=True)
    def test_enterprise_login(self, mock_github_login):
        """Test that correct list of arguments passed"""
        gh_fixture._login('my_api_token', url="https://github.company.com")

    @mock.patch('fixtures_git.github.github.login', autospec=True)
    def test_default_login(self, mock_github_login):
        """Test that correct list of arguments passed"""
        gh_fixture._login('my_api_token')


class TestGithubRepoFixture(testtools.TestCase):

    @mock.patch('fixtures_git.github._login',
                mock.Mock(return_value=mock.Mock()))
    def test_tempname_default(self):

        gh_repo = gh_fixture.GithubRepoFixture('owner', 'token')
        gh_repo.setUp()
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.StartsWith('workflow-test-')
        )
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.NotEquals('workflow-test-')
        )

    @mock.patch('fixtures_git.github._login',
                mock.Mock(return_value=mock.Mock()))
    def test_tempname_custom(self):

        template = 'my-custom-tmp-XXXXXX-template'
        gh_repo = gh_fixture.GithubRepoFixture('owner', 'token',
                                               name_template=template)
        gh_repo.setUp()
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.StartsWith(template.split('XXXXXX')[0])
        )
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.EndsWith(template.split('XXXXXX')[1])
        )

    @mock.patch('fixtures_git.github._login',
                mock.Mock(return_value=mock.Mock()))
    def test_tempname_no_suffix_in_template(self):

        template = 'my-custom-tmp-'
        gh_repo = gh_fixture.GithubRepoFixture('owner', 'token',
                                               name_template=template)
        gh_repo.setUp()
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.StartsWith(template)
        )
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.Not(
                testtools.matchers.Equals(template.split('XXXXXX')[-1]))
        )
        self.assertThat(
            gh_repo.repo_name.split('-')[-1],
            testtools.matchers.MatchesRegex('[a-f0-9]{8}')
        )

    @mock.patch('fixtures_git.github._login',
                mock.Mock(return_value=mock.Mock()))
    def test_tempname_exact_string(self):

        name = 'my-custom-tmp'
        gh_repo = gh_fixture.GithubRepoFixture('owner', 'token')
        gh_repo.repo_name = name
        gh_repo.setUp()
        self.assertThat(
            gh_repo.repo_name,
            testtools.matchers.Equals(name)
        )
